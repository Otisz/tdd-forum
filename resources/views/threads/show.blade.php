@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 mb-3">
                <div class="card">
                    <div class="card-header">
                        <a href="#">{{ $thread->creator->name }}</a> posted:
                        {{ $thread->title }}
                    </div>

                    <div class="card-body">
                        {{ $thread->body }}
                    </div>
                </div>
            </div>
        </div>

        @auth
            <div class="row justify-content-center">
                <div class="col-md-8 mb-3">
                    <form method="POST" action="{{ route('threads.replies.store', $thread->id) }}">

                        @csrf

                        <div class="form-group">

                            <textarea name="body" class="form-control" placeholder="Have something to say?"></textarea>

                        </div>

                        <button type="submit" class="btn btn-default">Post</button>

                    </form>
                </div>
            </div>
        @else
            <p class="text-center">Please <a href="{{ route('login') }}">sign in</a> to participate in this discussion.</p>
        @endauth

        <div class="row justify-content-center">
            <div class="col-md-8">
                @foreach($thread->replies as $reply)
                    @include('threads.reply')
                @endforeach
            </div>
        </div>
    </div>
@endsection
