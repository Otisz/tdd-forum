<?php

namespace App\Http\Controllers;

use App\Thread;
use Auth;
use Illuminate\Http\Request;

class RepliesController extends Controller
{
    /**
     * Create a new RepliesController instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Persist a new reply.
     *
     * @param \Illuminate\Http\Request $request
     * @param  Thread $thread
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, Thread $thread)
    {
        $thread->addReply([
            'user_id' => Auth::id(),
            'body'    => $request->get('body'),
        ]);

        return back();
    }
}
